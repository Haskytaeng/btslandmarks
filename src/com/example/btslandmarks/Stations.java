package com.example.btslandmarks;

import android.provider.BaseColumns;

public interface Stations extends BaseColumns{
	
	public final static String TABLE_NAME = "stations";
	public final static String NAME = "name";
	public final static String NO = "no";
	public final static String LINE = "line";
	public final static String NEXT1 = "next1";
	public final static String NEXT2 = "next2";

}
