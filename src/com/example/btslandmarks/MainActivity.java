package com.example.btslandmarks;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button b1 = (Button)findViewById(R.id.button1);
        b1.setOnClickListener(this);
        Button b2 = (Button)findViewById(R.id.button2);
        b2.setOnClickListener(this);
        
        Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/TCM.TTF");
        TextView tv1 = (TextView)findViewById(R.id.textView1);
        tv1.setTypeface(typeFace);
        b1.setTypeface(typeFace);
        b2.setTypeface(typeFace);
        b1.setBackgroundColor(Color.rgb(193, 149, 225));
        b2.setBackgroundColor(Color.rgb(193, 149, 225));
        b1.setTextColor(Color.rgb(64, 5, 0));
        b2.setTextColor(Color.rgb(64, 5, 0));
        
       try{
    	   ImageView iv = (ImageView)findViewById(R.id.imageView1);
    	   Drawable  drawable  = getResources().getDrawable(R.drawable.head);
           iv.setImageDrawable(drawable);
       }
       catch(Exception e){
    	   e.printStackTrace();
       }
        
    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	final static int code = 5555;

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.button1){
			Intent i = new Intent(this, SelectSukhumvitActivity.class);
			startActivity(i);
		}
		else{
			Intent i = new Intent(this, SelectSilomActivity.class);
			startActivity(i);
		}
		
	}

}
