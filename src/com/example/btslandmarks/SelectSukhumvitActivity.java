package com.example.btslandmarks;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleCursorAdapter;

public class SelectSukhumvitActivity extends Activity implements OnItemClickListener , Stations , Landmarks {
	
	StationsHelper stations;
	ListView lv;
	SimpleCursorAdapter adapter;
	Cursor cursor;

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_sukhumvit);
		
		Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/TCM.TTF");
        TextView tv1 = (TextView)findViewById(R.id.textView1);
        tv1.setTypeface(typeFace);
		
		stations = new StationsHelper(this);
		lv = (ListView) findViewById(R.id.listView1);
		cursor = retrieveData();
		adapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_2, 
				cursor, 
				new String[] { NAME, NO }, 
				new int[] { android.R.id.text1, android.R.id.text2 });
		
		
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
	}

	
	public Cursor retrieveData() {
		Cursor c = null;
		SQLiteDatabase db = stations.getReadableDatabase();
		c = db.query(TABLE_NAME,
				new String[] { _ID, NAME, NO, LINE, NEXT1, NEXT2 }, 
				"LINE = ?", 
				new String[] {"Sukhumvit"},
				null, null, NO + " ASC");
		return c;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_sukhumvit, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> a, View v, int pos, long id) {
		cursor.moveToPosition(pos);
		String name = cursor.getString(1);
		String line = cursor.getString(3);
		String next1 = cursor.getString(4);
		String next2 = cursor.getString(5);
		
		Intent i = new Intent(this, StationDetailActivity.class);
		i.putExtra(NAME, name);
		i.putExtra(LINE, line);
		i.putExtra(NEXT1, next1);
		i.putExtra(NEXT2, next2);
		startActivity(i);
		
	}

}
