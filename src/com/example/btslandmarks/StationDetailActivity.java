package com.example.btslandmarks;


import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.view.Menu;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class StationDetailActivity extends Activity implements Stations , Landmarks {

	LandmarksHelper landmarks;
	ListView lv;
	SimpleCursorAdapter adapter;
	Cursor cursor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_station_detail);
		
		Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/TCM.TTF");
		
		TextView tv1 = (TextView)findViewById(R.id.textView1);
		TextView tv2 = (TextView)findViewById(R.id.textView2);
		TextView tv3 = (TextView)findViewById(R.id.textView3);
		TextView tv4 = (TextView)findViewById(R.id.textView4);
		TextView tv5 = (TextView)findViewById(R.id.textView5);
		TextView tv6 = (TextView)findViewById(R.id.textView6);
		TextView tv7 = (TextView)findViewById(R.id.textView7);
		ListView lv1 = (ListView)findViewById(R.id.listView1);
		tv1.setTypeface(typeFace);
		tv2.setTypeface(typeFace);
		tv3.setTypeface(typeFace);
		tv4.setTypeface(typeFace);
		tv5.setTypeface(typeFace);
		tv6.setTypeface(typeFace);
		tv7.setTypeface(typeFace);
		
		
		Bundle extras = this.getIntent().getExtras();
		tv1.setText(extras.getString(NAME));
		tv3.setText(extras.getString(LINE));
		tv5.setText(extras.getString(NEXT1));
		tv6.setText(extras.getString(NEXT2));
		
		landmarks = new LandmarksHelper(this);
		cursor = retrieveData();
		adapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_2, 
				cursor, 
				new String[] { LNAME, DETAIL }, 
				new int[] { android.R.id.text1, android.R.id.text2});
		
		
		lv1.setAdapter(adapter);
	}
	
	public Cursor retrieveData() {
		Cursor c = null;
		Bundle extras = this.getIntent().getExtras();
		SQLiteDatabase db = landmarks.getReadableDatabase();
		c = db.query(TABLE_LNAME,
				new String[] { _ID, LNAME, STATION, DETAIL }, 
				"STATION = ?", 
				new String[] {extras.getString(NAME)},
				null, null, LNAME + " ASC");
		return c;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.station_detail, menu);
		return true;
	}

}
