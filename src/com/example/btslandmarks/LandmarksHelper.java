package com.example.btslandmarks;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class LandmarksHelper extends SQLiteOpenHelper implements Landmarks {
	
	public final static String DATABASE_NAME = "landmarks.db";
	public final static int DATABASE_VERSION = 3;

	public LandmarksHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE %s ("
				+ "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "%s TEXT NOT NULL, %s TEXT, %s TEXT);";

		db.execSQL(String.format(sql, TABLE_LNAME, _ID, LNAME, STATION,
						DETAIL));
		insertSampleData(db);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LNAME);
		onCreate(db);
		
	}
	
	public void insertSampleData(SQLiteDatabase db) {
		ContentValues v;

		v = new ContentValues();
		v.put(LNAME, "Chatuchak Park");
		v.put(STATION, "Mo Chit");
		v.put(DETAIL, "Chatuchak Park is one of the other public parks in Bangkok. Constraction began in 1975 on land donated by State Railway of Thailand. The park has an area of 0.0304 square kilometers.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Chatuchak market");
		v.put(STATION, "Mo Chit");
		v.put(DETAIL, "Weekend market (Frequently called J.J) in Bangkok it is the largest market in Thailand and the world’s weekend market. It still open on Saturday and Sunday.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Children's Discovery Museum");
		v.put(STATION, "Mo Chit");
		v.put(DETAIL, "It is a large complex with eight galleries with 123 exhibits and activities. The goal is to learn children in a fun way about human life, science, culture, society, nature and the environment, and there is an exhibition in honor of HM Queen Sirikit. ");
		db.insertOrThrow(TABLE_LNAME, null, v);

		v = new ContentValues();
		v.put(LNAME, "Sam Sen Nai Philatelic Museum");
		v.put(STATION, "Saphan Kwai");
		v.put(DETAIL, "This museum displays the history of the Thai postal service and the development of Thai stamps throughout history. There is a library with books about stamps, and you can buy stamps at the ground floor.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Paolo Memorial hospital");
		v.put(STATION, "Saphan Kwai");
		v.put(DETAIL, "Paolo Memorial hospital is a private hospital. Today it offers medical services by using state-of-the-art scientific methods and equipment, and comprised of well-trained medical personnel in order to act as a center for healthcare services.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Department of Land Transport");
		v.put(STATION, "Saphan Kwai");
		v.put(DETAIL, "The Department of Land Transport is responsible for the systematization and regulation of land transport by conducting the monitoring and inspection to ensure the smooth running and the conformity with land transport relevant rules and regulations. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "La Villa");
		v.put(STATION, "Ari");
		v.put(DETAIL, "This three-storey community mall brings a slice of Thonglor chic to SoiAree. La Villa caters to the contemporary lifestyle of its clientel such as iStudio, Japanese manga store, Wine Connection, After You , iberry, Greyhound cafe, Starbucks and, of course, Villa Market.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Ministry of Finance");
		v.put(STATION, "Ari");
		v.put(DETAIL, "The management of national establishment of the Royal Treasury Department in 1448. It was one of the four principal government agencies decreed to collect various forms of taxes and import duties and to operate the Crown's warehouse business and merchant fleet.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Office of the Auditor General of Thailand");
		v.put(STATION, "Ari");
		v.put(DETAIL, "The first enactment known in the history of Thailand on audit and control of public finance dated back to 1875 when the Royal Audit Office created by Royal Decree was proclaimed by the late King Chulalongkorn.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Phayathai 2 Hospital");
		v.put(STATION, "Sanam Pao");
		v.put(DETAIL, "The hospital is composed of 2 buildings and was established with the premise of promoting health care under the philosophy of public health care, concern for human values taking care of those who suffer from sickness with honor and dedication. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Royal Thai Army Radio and Television Channel 5");
		v.put(STATION, "Sanam Pao");
		v.put(DETAIL, "The public's perception is that the Station has been around for much longer than this. Broadcast signals go through a network of 31 transmitters dotted around the country. Daily transmission is 24 hours, but regular programs begins at 5:00 am until 3:00 am the next day. Between 3:00 am and 5:00 am broadcasts teleshopping.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "King Power Complex");
		v.put(STATION, "Victory Monument");
		v.put(DETAIL, "King Power offers two floors of exclusive duty-free shopping. Find high-quality Thai products like jewelry, fashion, art, crafts, homeware and spa products on the first floor. The second floor features an extensive selection of world-renowned luxury brands.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Bangkok Doll Factory & Museum ");
		v.put(STATION, "Victory Monument");
		v.put(DETAIL, "The Bangkok Doll Factory & Museum houses an interesting collection of Thai and foreign dolls made by a local factory, making it fun for Thai culture enthusiasts and children alike. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Rang Nam Beef Noodles");
		v.put(STATION, "Victory Monument");
		v.put(DETAIL, "The simple all-beef menu ranges from simple broth with meat balls to any combination possible as long as it’s on the day’s menu. Expect to wait for a seat, especially if you go near the lunch hours, as there are only eight tables inside.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Baiyoke Tower");
		v.put(STATION, "Phaya Thai");
		v.put(DETAIL, "It is one thing to see the Bangkok skyline from your hotel window, but watching it from the 84th-floor outdoor revolving deck is entirely another. Up top, the cool breeze, the excitement, the open space and the sense that you are on top of one of Thailand’s tallest buildings make all the difference.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Suan Pakkad Palace");
		v.put(STATION, "Phaya Thai");
		v.put(DETAIL, "Suan Pakkad Palace is a place to find visions of Thailand you thought long since vanished in Bangkok. Its name means 'cabbage patch' Today, however, it's a well-tended tropical garden with serene ponds surrounding eight traditional Thai houses.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Aksra Theater");
		v.put(STATION, "Phaya Thai");
		v.put(DETAIL, "The Bangkok Traditional Puppet Show at Aksra Theatre is one of Bangkok’s must-see attractions. Taking place on an elaborate stage, folk tales are told through the classic art of puppetry. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Pantip Plaza");
		v.put(STATION, "Ratchathewi");
		v.put(DETAIL, "To put it simply, if you are looking for electronics equipment in Bangkok.Even just for the experience it is well worth your time to explore the bright and flashing stalls and extensive variety of IT products offered, packed from floor to ceiling.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Platinum Fashion Mall");
		v.put(STATION, "Ratchathewi");
		v.put(DETAIL, "Platinum Fashion Mallfor wholesale clothing, fashion accessories and off-the-wall street wear in Thailand.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Pratunam Market");
		v.put(STATION, "Ratchathewi");
		v.put(DETAIL, "Pratunam Market is outdoor shopping bazaar jam-packed with all kinds of clothing stalls imaginable.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Siam Paragon");
		v.put(STATION, "Siam");
		v.put(DETAIL, "Siam Paragon is an immensely popular shopping mall housing a host of international high-end fashion brands, Southeast Asia’s largest aquarium, a 16-screen Cineplex, and a comprehensive selection of world cuisine. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Siam Discovery ");
		v.put(STATION, "Siam");
		v.put(DETAIL, "Siam Discovery collection of shops here caters to young urbanites and families. Standing among its glittering neighbour, it may not have that ‘wow’ effect but offers a good selection of designer fashion outlets and cutting-edge brands. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Siam Center ");
		v.put(STATION, "Siam");
		v.put(DETAIL, "Over 200 international and Thai brands including the boutiques of some brilliant promising local designers as well as renowned Thai couturiers. On top of that, the slick renovations have taken their fresh ideology out of the stores and into the corridors.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Madame Tussauds");
		v.put(STATION, "Siam");
		v.put(DETAIL, "Madame Tussauds has stepped down from the pedestal and embraced her visitors with a revolutionary concept – the wax museum with exhibits you can touch, hug, play with and even kiss. ");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "CentralWorld");
		v.put(STATION, "Chit Lom");
		v.put(DETAIL, "CentralWorld mega-shopping complex offers one of the most exciting shopping experiences in Bangkok. It has everything from brand name clothing boutiques, funky fashion, high-tech gadgets, bookshops and designer furniture to imported groceries, a lineup of banks, beauty salons, gourmet eateries and even an ice-skating rink.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Central Chitlom Department Store");
		v.put(STATION, "Chit Lom");
		v.put(DETAIL, "Central Chitlom is a seven-storey department store with a modern, friendly ambience. Well-stocked, each floor has a straightforward layout dedicated to urban lifestyles.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Erawan Shrine ");
		v.put(STATION, "Chit Lom");
		v.put(DETAIL, "Erawan Shrine in Bangkok is Brahman, not strictly Buddhist. And yet, this famous shrine attracts more visitors than many of the city's temples. It was erected during the mid 1950sthe first stages of the construction were beset with so many problems that superstitious labourers refused to continue unless the land spirits were appeased.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Shrine of Goddess Tubtim");
		v.put(STATION, "Phloen Chit");
		v.put(DETAIL, "Literally hundreds of penises (phalluses) - from small wooden carvings to big stone sculptures that stand ten feet tall and decorated with ribbons  make this shrine quite unique. It honors Chao Mae Tubtim, a female fertility spirit.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Novotel Bangkok Ploenchit Sukhumvit Hotel");
		v.put(STATION, "Phloen Chit");
		v.put(DETAIL, "Novotel Bangkok PloenchitSukhumvit is situated at the heart of Bangkok’s city centre, in the prestigious Ploenchit-Chidlom area. The hotel is easily accessible by major roads, the airport expressway and is a stone’s throw from the Ploenchit BTS Skytrain.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "The Ambassador Hotel Bangkok");
		v.put(STATION, "Nana");
		v.put(DETAIL, "Set in the heart of SukhumvitSoi 11, The Ambassador Hotel is surrounded by a wealth of shopping, nightlife and restaurants. Adjacent to the hotel’s Main Wing, Am Plaza is an onsite retail therapy.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Amari Boulevard Bangkok Hotel");
		v.put(STATION, "Nana");
		v.put(DETAIL, "The Amari Boulevard Bangkok is located on Sukhumvit Road Soi 5, in the very heart of Bangkok commercial and shopping district, with immediate access to the Airport expressway and Nana BTS skytrainstation.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Terminal 21 ");
		v.put(STATION, "Asok");
		v.put(DETAIL, "Terminal 21 brings the dream of traveling the world to downtown Bangkok, with its cutting-edge concept that places several world-famous cities under one roof. Find yourself wandering around a maze of shops in Tokyo City, sauntering down London’s Carnaby Street, bagging a bargain in an Istanbul zouk, or shopping for something to fill your growling stomach at the Fisherman’s Wharf in San Francisco.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Arirang Korean Restaurant");
		v.put(STATION, "Asok");
		v.put(DETAIL, "Arirang is easy to spot when approaching from the Asoke intersection. Inside, the all wood interiors give the place a cozy. Find both Korean barbecue and a-la-carte dishes on the multi-page menu, but most people come here for the barbecue experience using charcoal grill.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Doo Rae Korean Restaurant");
		v.put(STATION, "Asok");
		v.put(DETAIL, "This three-storey Korean barbecue house is one among many housed inside Sukhumvit Plaza (Korean Town), but Doo Rae stands out in terms of service and a down-to-earth ambience.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "The Emporium Department Store");
		v.put(STATION, "Phrom Phong");
		v.put(DETAIL, "The Emporium is like a glossy magazine come to life. Behold the seven layers of shopper's paradise, where the first three floors are dedicated solely to fashion. Exclusive world-class brand names.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Benjasiri Park");
		v.put(STATION, "Phrom Phong");
		v.put(DETAIL, "One of the most endearing features of Bangkok is the city's numerous parks where abundant greenery sprouts amongst towering sky scrapers and congested roads. Check out our Bangkok Parks and Activities page for more information on parks in the city.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "K Village");
		v.put(STATION, "Phrom Phong");
		v.put(DETAIL, "K Village is a modern-looking mall featuring some 100 air-conditioned shops in its five zones, including Fashion & Accessories, Cafe & Restaurants, Health & Beauty, Lifestyle & Technology and Gourmet Market");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "J-Avenue");
		v.put(STATION, "Thong Lo");
		v.put(DETAIL, "This lifestyle outdoor mall was among the first open and still is thriving on Thonglor. It  features an eclectic mix of restaurants, cafes, fast food outlets, IT stores, banks and beauty salons as well as Villa Market and Major Bowl Hit.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "The Face Restaurant");
		v.put(STATION, "Thong Lo");
		v.put(DETAIL, "Named after the proprietor's chance encounter with a wood carving of a Buddha face in Chiang Mai, this teakwood venue is not one you will forget in a hurry. Comprising three restaurants – Thai, Japanese and Indian. The Face fuses traditional Thai décor with international cuisine.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Gateway Ekkamai");
		v.put(STATION, "Ekkamai");
		v.put(DETAIL, "Gateway Ekkamai mall aims to offer an alternative shopping experience from the other cookie-cutter centers across Bangkok. The mall is meant to the Japanese lifestyle that Thai people embrace readily with a mix of restaurants, clothes stores.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Museum of Science and Planetarium");
		v.put(STATION, "Ekkamai");
		v.put(DETAIL, "Here youngsters can learn about science and technology, natural history, the environment and deep space in informal surroundings. The Science Museum aims to open the door on scientific experimentation and discovery with special events, lectures and discussions on science, astrology and related subjects.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Akiyoshi Japanese Restaurant");
		v.put(STATION, "Phra Khanong");
		v.put(DETAIL, "Japanese shabu-shabu or sukiyaki or both. Choose from the a la carte menu or all-you-can-eat style. A selection of sushi, sashimi, grilled fish and meatbut it’s better to stick to either the shabu-shabu or sukiyaki. The restaurant is easy to find.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Sabai-Sabai@Sukhumvit Hotel");
		v.put(STATION, "Phra Khanong");
		v.put(DETAIL, "Sabai-Sabai@Sukhumvit Resort & Spa ( Boutique Hotel ) - Resort & Spa takes pride in being one of the hotels located at the centre of the Sukhumvit Road. It is very convenient to go by BTS to the restaurant and the Entertainment at Nana.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "On Nut Square");
		v.put(STATION, "On Nut");
		v.put(DETAIL, "On Nut Square is a mini bazaar with an array of street fashion, personal accessories, trinkets and knick-knack. When you walk out to the BTS , On Nut Square will be on the left hand side.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "On Nut Food Market");
		v.put(STATION, "On Nut");
		v.put(DETAIL, "–	On Nut Food Market is outdoor food court is ideal for grabbing a quick bite with a glass of cold beer and have many interesting show in the evening .It is nearly On Nut Square and opposite Big C On Nut.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Rod Dee");
		v.put(STATION, "Bang Chak");
		v.put(DETAIL, "Rod Dee (SukhumvitSoi 95) – Chinese eatery with roasted duck, barbecued pork, wonton soup and pan-fried noodles with gravy.This restaurant is very delicious and very interesting because this restaurant decoration and interior with Chinese style");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Guang Chao Sukiyaki");
		v.put(STATION, "Bang Chak");
		v.put(DETAIL, "Guang Chao Sukiyaki (at the foot of the BTS station) – Chinese-style hotpot, roasted duck, barbecued pork and shark fin soup.This restaurant present about Hilum Sukiyaki.This is sukiyaki Chinese style .The famous menu of this restaurant is “ roasted duck ”.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "HuaSeng Hong Chinese Restaurant");
		v.put(STATION, "Punnawithi");
		v.put(DETAIL, "HuaSeng Hong Chinese Restaurant(SukhumvitSoi 101)  is Thai and Chinese style seafood, vegetarian and dim sum ,The Location near Punnawithi BTS Station. It ia about 300 metres from the station.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Sam Bai Tao");
		v.put(STATION, "Punnawithi");
		v.put(DETAIL, "Sam Bai Tao (inside SukhumvitSoi 101)  is affordable Vietnamese cuisine and Thai , in an open-air shop-house setting. The most famous Vietnamese cuisine is “Nam Nung”.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Saen Aroi Pochana");
		v.put(STATION, "Udom Suk");
		v.put(DETAIL, "SaenAroiPochana it is location on UdomsukSoi 49 this open-air restaurant is perfect for enjoying made-to-order Thai and Chinese fare.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "SomboonPochana");
		v.put(STATION, "Udom Suk");
		v.put(DETAIL, "SomboonPochana it is location on UdomsukSoi 60 well established restaurant famous for its curry crab and Chinese-style seafood dishes.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Paradise Park");
		v.put(STATION, "Bang Na");
		v.put(DETAIL, "Paradise Park brings high street shopping and contemporary lifestyle to eastern Bangkok. It has five floors, with over 700 retail shops, a Cineplex with digital and 3D screens as well as IT gadgets, food, banking, home décor and wellness. Located on Srinakarin Road");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "IKEA");
		v.put(STATION, "Bang Na");
		v.put(DETAIL, "The Bangna branch is IKEA’s first flagship store in Thailand and features all the brand’s signature services, facilities and a gallery-style showroom. The store layout enforces self-guided shopping: you enter one way pay for your purchase and exit the store the other way.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Seacon Square");
		v.put(STATION, "Bang Na");
		v.put(DETAIL, "The mall features a mini-golf course and a 14-theatre multiplex cinema, as well as a motion 'simulator' theatre. Come here if you want to let your kids loose, while you do your shopping.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "APT Bearing Mall");
		v.put(STATION, "Bearing");
		v.put(DETAIL, "APT Bearing Mall (Exit 2) – outdoor community mall with individual shops selling clothing, fashion accessories, shoes, leather bags and snacks.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Saeb Tee Som Tam");
		v.put(STATION, "Bearing");
		v.put(DETAIL, "Saeb Tee Som Tam  for tasty northeastern cuisine, som tam, larb and, the signature menu, earthen jar-baked chicken.");
		db.insertOrThrow(TABLE_LNAME, null, v);

		v = new ContentValues();
		v.put(LNAME, "MBK Shopping Center ");
		v.put(STATION, "National Stadium");
		v.put(DETAIL, "The multi-storey MBK is probably Bangkok's most legendary shopping mall, popular with both tourists and locals. Shops that sell everything from clothing, fashion accessories, handbags, leather products ,mobile phones, electric appliances, cameras, stationery and DVDs.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Jim Thompson's cafe");
		v.put(STATION, "National Stadium");
		v.put(DETAIL, "The cafe is surrounded by water and greenery along with exotic butterflies and birds. The café features both Thai and western dishes, cooked and presented to perfection with pretty good coffee, too.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "ERAWAN SHRINE IN BANGKOK");
		v.put(STATION, "Ratchadamri");
		v.put(DETAIL, "Erawan Shrine in Bangkok is Brahman, not strictly Buddhist. And yet, this famous shrine attracts more visitors than many of the city's temples. It was created during the mid 1950s.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Amarin Plaza");
		v.put(STATION, "Ratchadamri");
		v.put(DETAIL, "The stylish Amarin Plaza is linked to Erawan Bangkok via a skybridge. The five-storey shopping plaza has an almost festive aura to it, with highly desirable brand names such as Guy Laroche, Valentino and Crocodile.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Lumpini Park");
		v.put(STATION, "Sala Daeng");
		v.put(DETAIL, "Lumpini Park is an inner-city haven of tranquility.Named after the birthplace of the Lord Buddha in Nepal, the park is more than half a million square kilometres big, and the habitat of various flora and fauna.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Patpong Night market");
		v.put(STATION, "Sala Daeng");
		v.put(DETAIL, "After recently cleaning up its act, Patpong's become one of the 'must' night shopping destinations for avoid shoppers. Found in the built-up area known as Silom, the place is always busy and chaotic with all the commotion from the Go-Go bars.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "DusitThani Hotel Bangkok");
		v.put(STATION, "Sala Daeng");
		v.put(DETAIL, "Its international luxury hotel standards delivered with distinctive Thai artistry and graciousness personify the earthly manifestation of its name which means in Thai, a town in heaven.Ideally located in the heart of Bangkok, known as the 'City of Angels'.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Daimasu Japanese Restaurant");
		v.put(STATION, "Sala Daeng");
		v.put(DETAIL, "You can travel to Tokyo without having to leave Bangkok! This tiny Japanese restaurant just off Surawong Road, is exactly what an Izakaya looks like in Japan. A warm and lively little place serving a large choice of grilled meats, fish and veggies along with salads, snacks and lots of drinks.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Sofitel Silom Hotel");
		v.put(STATION, "Chong Nonsi");
		v.put(DETAIL, "A 30-storey urban design hotel in Bangkok, created by a reputable Thai Architect and five Thai interior designers, linked to French elegance by Monsieur Christian Lacroix.");
		db.insertOrThrow(TABLE_LNAME, null, v);
			
		v = new ContentValues();
		v.put(LNAME, "Salaisap Market");
		v.put(STATION, "Chong Nonsi");
		v.put(DETAIL, "Salaisap Market or Lalaisup Market is outdoor shopping paradise for all things inexpensive, from delicious street food to shoes, clothing , watch, Brand name  and all kinds of knickknacks that you can imaginable.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "M.r. Kukrit House Museum In Bangkok");
		v.put(STATION, "Chong Nonsi");
		v.put(DETAIL, "M.R. Kukrit’s Heritage Home offers a pleasant journey into the life of Thailand’s former prime minister and multi-talented personality. He spend more than 40 novels, stage plays, short stories and poems as well as actively promoting traditional Thai culture.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Sri Mariamman Hindu Temple");
		v.put(STATION, "Surasak");
		v.put(DETAIL, "This Hindu temple dedicated to the Goddess Mariamman was built by Tamil immigrants in the 1860s. Renowned for her power to protect against disease and death, it remains a popular place of worship for Silom's long-established Indian community.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Kathmandu Art Gallery");
		v.put(STATION, "Surasak");
		v.put(DETAIL, "This period-style shop-house gallery is a contemporary take on Thai art and culture. Featuring six photo exhibits per year, each intends to be thought-provoking and promotes both upcoming and well-established talents.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Blue Elephant School");
		v.put(STATION, "Surasak");
		v.put(DETAIL, "This well-known chain of Thai restaurants already had branches in Europe and the Middle East before opening a venue in Bangkok. You might know the Blue Elephant for being one of Bangkok’s most famous cooking schools.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "King Rama I Monument");
		v.put(STATION, "Saphan Taksin");
		v.put(DETAIL, "King Rama I was built to mark the city's 150th anniversary in 1932. King Rama I was the first king in the Chakri Dynasty that continues to this day. He ascended to the throne on April 6, 1782, and died 27 years later.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Royal Barges Museum");
		v.put(STATION, "Saphan Taksin");
		v.put(DETAIL, "Once upon a time in Bangkok the Chao Phraya River and its tributaries were the most important avenue of communication, boats the main mode of transport for all, even royalty.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Arun Temple");
		v.put(STATION, "Krung Thon Buri");
		v.put(DETAIL, "WatArun, locally known as WatChaeng, The spire (prang) of Wat Arun on the bank of Chao Phraya River is one of Bangkok’s world-famous landmarks. It has an imposing spire over 70 metres high, beautifully decorated with tiny pieces of color glasess.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Temple of the Emerald Buddha");
		v.put(STATION, "Krung Thon Buri");
		v.put(DETAIL, "WatPhraKaew or the Temple of the Emerald Buddha (officially known as WatPhra Sri RattanaSatsadaram) is regarded as the most important Buddhist temple in Thailand. Located in the historic centre of Bangkok.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "KhinLomChomSaphan");
		v.put(STATION, "Krung Thon Buri");
		v.put(DETAIL, "The name of this charming open-air restaurant which consists of three pavilions and a lotus pond, roughly translates as chilling out and admiring the bridge – the restaurant has great views of the Ramah VIII Bridge.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "King Taksin the Great Monument");
		v.put(STATION, "Wongwian Yai");
		v.put(DETAIL, "King Taksin the Great Monument a towering landmark on the Thonburi side.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "WongwianYai Market");
		v.put(STATION, "Wongwian Yai");
		v.put(DETAIL, "WongwianYai Market is sell about fresh fruit, vegetables, snacks and ready-to-eat meals.");
		db.insertOrThrow(TABLE_LNAME, null, v);
		
		v = new ContentValues();
		v.put(LNAME, "Saeb Tee Som Tam");
		v.put(STATION, "Chong Nonsi");
		v.put(DETAIL, "Shopping Mall");
		db.insertOrThrow(TABLE_LNAME, null, v);
	}

}
