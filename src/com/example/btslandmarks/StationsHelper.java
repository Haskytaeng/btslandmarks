package com.example.btslandmarks;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class StationsHelper extends SQLiteOpenHelper implements Stations {
	
	public final static String DATABASE_NAME = "stations.db";
	public final static int DATABASE_VERSION = 3;

	public StationsHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE %s ("
				+ "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "%s TEXT NOT NULL, %s TEXT, %s TEXT, %s TEXT , %s TEXT);";

		db.execSQL(String.format(sql, TABLE_NAME, _ID, NAME, NO,
						LINE, NEXT1, NEXT2));
		insertSampleData(db);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
		
	}
	
	public void insertSampleData(SQLiteDatabase db) {
		ContentValues v;

		v = new ContentValues();
		v.put(NAME, "Mo Chit");
		v.put(NO, "N8");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Saphankwai");
		v.put(NEXT2, "");
		db.insertOrThrow(TABLE_NAME, null, v);

		v = new ContentValues();
		v.put(NAME, "Saphan Kwai");
		v.put(NO, "N7");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Mo Chit");
		v.put(NEXT2, "Ari");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Ari");
		v.put(NO, "N5");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Saphankwai");
		v.put(NEXT2, "Sanam Pao");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Sanam Pao");
		v.put(NO, "N4");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Ari");
		v.put(NEXT2, "Victory Monument");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Victory Monument");
		v.put(NO, "N3");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Sanam Pao");
		v.put(NEXT2, "Phaya Thai");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Phaya Thai");
		v.put(NO, "N2");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Victory Monument");
		v.put(NEXT2, "Ratchathewi");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Ratchathewi");
		v.put(NO, "N1");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Phaya Thai");
		v.put(NEXT2, "Siam");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Siam");
		v.put(NO, "CENTER");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Ratchathewi");
		v.put(NEXT2, "Chit Lom");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Chit Lom");
		v.put(NO, "E1");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Siam");
		v.put(NEXT2, "Phloen Chit");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Phloen Chit");
		v.put(NO, "E2");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Chit Lom");
		v.put(NEXT2, "Nana");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Nana");
		v.put(NO, "E3");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Phloen Chit");
		v.put(NEXT2, "Asok");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Asok");
		v.put(NO, "E4");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Nana");
		v.put(NEXT2, "Phrom Phong");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Phrom Phong");
		v.put(NO, "E5");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Asok");
		v.put(NEXT2, "Thong Lo");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Thong Lo");
		v.put(NO, "E6");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Phrom Phong");
		v.put(NEXT2, "Ekkamai");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Ekkamai");
		v.put(NO, "E7");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Thong Lo");
		v.put(NEXT2, "Phra Khanong");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Phra Khanong");
		v.put(NO, "E8");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Ekkamai");
		v.put(NEXT2, "On Nut");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "On Nut");
		v.put(NO, "E9");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Phra Khanong");
		v.put(NEXT2, "Bang Chak");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Bang Chak");
		v.put(NO, "E10");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "On Nut");
		v.put(NEXT2, "Punnawithi");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Punnawithi");
		v.put(NO, "E11");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Bang Chak");
		v.put(NEXT2, "Udom Suk");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Udom Suk");
		v.put(NO, "E12");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Punnawithi");
		v.put(NEXT2, "Bang Na");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Bang Na");
		v.put(NO, "E13");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Udom Suk");
		v.put(NEXT2, "Bearing");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Bearing");
		v.put(NO, "E14");
		v.put(LINE, "Sukhumvit");
		v.put(NEXT1, "Bang Na");
		v.put(NEXT2, "");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		
		
		v = new ContentValues();
		v.put(NAME, "National Stadium");
		v.put(NO, "W2");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Siam");
		v.put(NEXT2, "");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Siam");
		v.put(NO, "CENTER");
		v.put(LINE, "Silom");
		v.put(NEXT1, "National Stadium");
		v.put(NEXT2, "Ratchadamri");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Ratchadamri");
		v.put(NO, "S1");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Siam");
		v.put(NEXT2, "Sala Daeng");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Sala Daeng");
		v.put(NO, "S2");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Ratchadamri");
		v.put(NEXT2, "Chong Nonsi");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Chong Nonsi");
		v.put(NO, "S3");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Sala Daeng");
		v.put(NEXT2, "Surasak");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Surasak");
		v.put(NO, "S5");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Chong Nonsi");
		v.put(NEXT2, "Saphan Taksin");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Saphan Taksin");
		v.put(NO, "S6");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Surasak");
		v.put(NEXT2, "Krung Thon Buri");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Krung Thon Buri");
		v.put(NO, "S7");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Saphan Taksin");
		v.put(NEXT2, "Wongwian Yai");
		db.insertOrThrow(TABLE_NAME, null, v);
		
		v = new ContentValues();
		v.put(NAME, "Wongwian Yai");
		v.put(NO, "S8");
		v.put(LINE, "Silom");
		v.put(NEXT1, "Kring Thon Buri");
		v.put(NEXT2, "");
		db.insertOrThrow(TABLE_NAME, null, v);

	}

}
