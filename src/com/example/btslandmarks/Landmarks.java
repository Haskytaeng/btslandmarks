package com.example.btslandmarks;

import android.provider.BaseColumns;

public interface Landmarks extends BaseColumns{

	public final static String TABLE_LNAME = "landmarks";
	public final static String LNAME = "lname";
	public final static String STATION = "station";
	public final static String DETAIL = "detail";
}
